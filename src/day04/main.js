//Load the libraries
var express = require("express");
var path = require("path");

var app = express();

app.set("port",process.env.APP_PORT | 3000);

app.get("/employee/:empNo",function(req, resp){
    var empNo = req.params.empNo;

    resp.type("text/html")
        .status(200)
        .send("");
        
});

app.get("/",function(req, resp){

    var userAgent = req.get("user-agent");

    resp.status(200);
    resp.type("text/html");
    resp.send("<h1>You are using </h1>");

})

app.listen(app.get("port"),function(){
    console.log("Application started at %s on port %d", new Date(),app.get("port"));

}
);

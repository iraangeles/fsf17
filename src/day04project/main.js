//Load the libraries
var express = require("express");
var path = require("path");

var app = express();

// ********************************************
var deckOfCards = function(shuf){
    var suit = ["Club","Spade","Diamond","Hearts"];
    var name = ["Ace","2","3","4","5","6","7","8","9","10","Jack","Queen","King"];
    var value = [1,2,3,4,5,6,7,8,9,10,10,10,10];

    var theDeck = [];
    // var shuf = 0;

    //Create the deck

    for (var s in suit){
        for (var n in name)
            theDeck.push({
                suit: suit[s],
                name: name[n],
                value: value[n]

            });

    }

    //Shuffle
    shuf = shuf || 3;
    for (var times = 0; times < shuf; times++)
        for (var i=0; i < theDeck.length; i++){
            var dst = parseInt(Math.random() * theDeck.length);
            var swapVal = theDeck[i];
            theDeck[i] = theDeck[dst];
            theDeck[dst] = swapVal;

        }

    return (theDeck);

}



// *******************************

var taitee = deckOfCards();
console.log(taitee[1]);



app.set("port",process.env.APP_PORT | 3000);

app.get("/cards",function(req, resp){
    var taitee = deckOfCards();

    resp.type("application/json")
        .status(200)
        .json(taitee);
        
});

app.get("/card/:cardNo",function(req, resp){
    var cardNo = req.params.cardNo;
    var taitee = deckOfCards();
    var tmpDeck = [];

    var cNo = parseInt(cardNo);

    switch(true){
        case (cNo >= 1 && cNo <= 52): 
        {
            for (var i = 0; i < cardNo; i++){
                tmpDeck.push(taitee[i]);
            }

            resp.type("text/plain")
                .status(200)
                .send(tmpDeck);

            break;
        }
        default:{
            resp.type("text/plain")
                .status(200)
                .send("Invalid Input");
            break;

        }

    }
    
        



});



app.listen(app.get("port"),function(){
    console.log("Application started at %s on port %d", new Date(),app.get("port"));

}
);



